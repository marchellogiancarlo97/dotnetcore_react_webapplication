# DotNetCore_React_WebApplication

This is a test application based on a SPA as Front End (Angular or React) and a set of REST API in NET Core as
Back end, SQL database as DB.
Purpose of the app is to visualize orders received by our company and statistics and edit an order status.